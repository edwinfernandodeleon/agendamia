-- DB AgendaIn6am
CREATE DATABASE IF NOT EXISTS AgendaMia;
USE AgendaMia;

CREATE TABLE Usuario (
	idUsuario INT NOT NULL AUTO_INCREMENT,
    usuario VARCHAR(30) NOT NULL,
    contrasena VARCHAR(30) NOT NULL,
    PRIMARY KEY (idUsuario)
);

CREATE TABLE Categoria(
	idCategoria INT NOT NULL AUTO_INCREMENT,
    nombreCategoria VARCHAR(30) NOT NULL,
    PRIMARY KEY (idCategoria)
);

CREATE TABLE Contacto(
	idContacto INT NOT NULL auto_increment,
    nombre VARCHAR(30) NOT NULL,
    apellido VARCHAR(30) NOT NULL,
    direccion VARCHAR(30) NOT NULL,
    telefono VARCHAR(12) NOT NULL,
    correo VARCHAR(40) NOT NULL,
    idCategoria INT NOT NULL,
    PRIMARY KEY (idContacto),
    FOREIGN KEY (idCategoria) REFERENCES Categoria (idCategoria)
);

CREATE TABLE UsuarioDetalle(
	idUsuarioDetalle INT NOT NULL AUTO_INCREMENT,
    idUsuario INT NOT NULL,
    idContacto INT NOT NULL,
    PRIMARY KEY (idUsuarioDetalle),
    FOREIGN KEY (idUsuario) REFERENCES Usuario(idUsuario),
    FOREIGN KEY (idContacto) REFERENCES Contacto(idContacto)
);

-- INSERTS
INSERT INTO Usuario(usuario, contrasena) values ("admin","admin");
INSERT INTO Usuario(usuario, contrasena) values ("a","a");
INSERT INTO Usuario(usuario, contrasena) values ("b","b");
INSERT INTO Usuario(usuario, contrasena) values ("c","d");
INSERT INTO Usuario(usuario, contrasena) values ("e","e");

INSERT INTO Categoria(nombreCategoria) values ("Familia");
INSERT INTO Categoria(nombreCategoria) values ("Compañeros");
INSERT INTO Categoria(nombreCategoria) values ("Universidad");
INSERT INTO Categoria(nombreCategoria) values ("Iglesia");
INSERT INTO Categoria(nombreCategoria) values ("Otros");

INSERT INTO Contacto (nombre, apellido, direccion, telefono, correo, idCategoria) VALUES ('Jessica', 'Bal', 'zona 18', '01', 'jessica@hotmail.com', '1');
INSERT INTO Contacto(nombre, apellido, direccion, telefono, correo, idCategoria) VALUES ('Dany', 'de León', 'zona 18', '02', 'dany@hotmaill.com', '2');
INSERT INTO Contacto (nombre, apellido, direccion, telefono, correo, idCategoria) VALUES ('Mamá', 'Véliz', 'zona 18', '03', 'riberta@gmail.com', '2');
INSERT INTO Contacto (nombre, apellido, direccion, telefono, correo, idCategoria) VALUES ('Fredy', 'Garcia', 'zona 18', '04', 'fredy@yahoo.com', '3');
INSERT INTO Contacto(nombre, apellido, direccion, telefono, correo, idCategoria) VALUES ('Didier', 'Domínguez', 'zona 21', '05', 'dadu@gmail.com', '4');

INSERT INTO UsuarioDetalle(idUsuario, idContacto) values (1, 1);
INSERT INTO UsuarioDetalle(idUsuario, idContacto) values (1, 2);
INSERT INTO UsuarioDetalle(idUsuario, idContacto) values (1, 3);
INSERT INTO UsuarioDetalle(idUsuario, idContacto) values (2 ,1);
INSERT INTO UsuarioDetalle(idUsuario, idContacto) values (2, 2);

-- VISTA DE UsuarioDetalle
-- Vista de filtros por usuario
CREATE VIEW Ver_Detalles AS (
SELECT a.idUsuarioDetalle, b.usuario as User, c.nombre as Contact
FROM UsuarioDetalle a
INNER JOIN Usuario b ON b.idUsuario = a.idUsuario
INNER JOIN Contacto c ON c.idContacto = a.idContacto
);


-- Vista de filtros por idUsuario
CREATE VIEW Ver_DetallesById AS (
SELECT a.idUsuarioDetalle, b.idUsuario as Usuario, c.nombre as nombre, c.apellido as apellido,
c.direccion as direccion, c.telefono as telefono, c.correo as correo,
c.idCategoria as categoria
FROM UsuarioDetalle a
INNER JOIN Usuario b ON b.idUsuario = a.idUsuario
INNER JOIN Contacto c ON c.idContacto = a.idContacto
);
-- ************** FIN FILTROS

-- Vista de filtros por idUsuario
CREATE VIEW Ver_DetallesId AS (
SELECT a.idUsuarioDetalle, b.idUsuario as User, c.nombre as Contact
FROM UsuarioDetalle a
INNER JOIN Usuario b ON b.idUsuario = a.idUsuario
INNER JOIN Contacto c ON c.idContacto = a.idContacto
);
-- ************** FIN FILTROS

-- PROCEDIMIENTOS ALMACENADOS Usuario
delimiter $
CREATE PROCEDURE Crear_Usuario (in ausuario varchar(30), in acontrasena varchar(30))
BEGIN
	IF (NOT EXISTS(SELECT * FROM Usuario WHERE usuario = ausuario AND contrasena = acontrasena)) THEN
		INSERT INTO Usuario (usuario, contrasena) VALUES (ausuario, acontrasena);
	END IF;
END
$

delimiter $
CREATE PROCEDURE Actualizar_Usuario (in aidUsuario int, in ausuario varchar(30), in acontrasena varchar(30))
BEGIN
	UPDATE Usuario SET  usuario = ausuario, contrasena = acontrasena WHERE idUsuario = aidUsuario;
END
$

delimiter $
CREATE PROCEDURE Eliminar_Usuario (in aidUsuario int)
BEGIN
	DELETE FROM Usuario WHERE idUsuario = aidUsuario;
END
$

-- LLAMAR PRODEDIMIENTOS ALMACENADOS
Call Nombre_Procedimiento ("par1","par2");

-- LLAMAR UNA VISTA
SELECT * FROM Nombre_Vista;
