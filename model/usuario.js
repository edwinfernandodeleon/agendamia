var database = require('./database');
var usuario = {};

usuario.selectAll = function(callback) {
  if(database) {
    database.query("SELECT * FROM Usuario",
    function(error, resultados) {
      if(error) {
        throw error;
      } else {
        callback(null, resultados);
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll

usuario.select = function(idUsuario, callback) {
  if(database) {
    var sql = "CALL Ver_Usuario (?)";
    database.query(sql, idUsuario,
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback(null, resultado);
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll

usuario.autenticar = function(data, callback) {
  if(database) {
    var sql = "SELECT * FROM Usuario WHERE usuario = ? AND contrasena = ?";
    database.query(sql, [data.usuario, data.contrasena],
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback(null, resultado);
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll


usuario.insert = function(data, callback) {
  if(database) {
    database.query("CALL Insertar_Usuario (?)", data,
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        var respuesta = {
          insertId: resultado.insertId,
          usuario: data.usuario,
          idUsuario: resultado.insertId
        };
        callback(null, respuesta);
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll

usuario.update = function(data, callback) {
  if(database) {
    var sql = "CALL Editar_Usuario (?, ?, ?)";
    database.query(sql,
    [data.usuario, data.contrasena, data.idUsuario],
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback(null, {"insertId": resultado.insertId});
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll

usuario.delete = function(idUsuario, callback) {
  if(database) {
    var sql = "CALL Eliminar_Usuario (?)";
    database.query(sql, idUsuario,
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback(null, {"Mensaje": "Eliminado"});
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll


module.exports = usuario;
